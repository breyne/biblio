# bibliographic-databases

*Curated BibTex files by theme.*

---

While literature review is passionating for some, starting from scratch is never fun. 
These files are bibliographic databases gathered along the way, with a focus on consistent and "proper" referencing. Comments and abstracts are removed but my (subjective) groups and tags are left. 

They are [BibTex](http://www.bibtex.org/Format/) formatted, build and meant to be used with software [JabRef](https://www.jabref.org/) in order to take advantage of the grouping and tagging features.

---


**`plastic-instabilities.bib`**: the Portevin--Le Chatelier effect in aluminum alloys: characterization, constitutive modeling and numerical simulation.

- Context: PhD, see [dissertation](https://tel.archives-ouvertes.fr/tel-02437346/file/B_REYNE.pdf)
- last update in October 2019
- grouping is about 85% done.